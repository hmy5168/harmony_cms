<?php

namespace app\controller;

use app\BaseController;

class Index extends BaseController
{
    public function index()
    {
        return '<h1>哈曼云CMS</h1><h2>' . date('Y-m-d H:i:s') . '</h2><p>PHP开源CMS系统，基于ThinkPHP6.0创建...</p><p>山东哈曼云信息科技有限公司</p>';
    }

    public function hello($name = 'ThinkPHP6')
    {
        return 'hello,' . $name;
    }
}
